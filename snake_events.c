//******************************************************************************
//	snake_events.c
//
//  Author:			Paul Roper, Brigham Young University
//  Revisions:		1.0		11/25/2012	RBX430-1
//
//******************************************************************************
//
#include "msp430.h"
#include <stdlib.h>
#include "RBX430-1.h"
#include "RBX430_lcd.h"
#include "snakelib.h"
#include "snake.h"

extern volatile uint16 sys_event;			// pending events
extern volatile uint16 move_cnt;			// snake speed

extern volatile enum modes game_mode;		// 0=idle, 1=play, 2=next
extern volatile uint16 score;				// current score
extern volatile uint16 level;				// current level (1-4)
extern volatile uint16 direction;			// current move direction
extern volatile uint16 time_remaining;		// time remaining
extern volatile uint16 object_length;		// items in index
volatile uint8 index_of_replacement;	// index of food to be replaced


extern volatile uint8 head;					// head index into snake array
extern volatile uint8 tail;					// tail index into snake array
extern SNAKE snake[MAX_SNAKE];				// snake segments
extern OBJECT *objects;						// objects array

extern const uint16 snake_text_image[];
extern const uint16 snake1_image[];
extern const uint16 king_snake_image[];

static uint8 move_right(SNAKE* head);		// move snake head right
static uint8 move_up(SNAKE* head);			// move snake head up
static uint8 move_left(SNAKE* head);		// move snake head left
static uint8 move_down(SNAKE* head);		// move snake head down
static void new_snake(uint16 length, uint8 dir);
static void delete_tail(void);
static void add_head(void);

//-- switch #1 event -----------------------------------------------------------
//
void SWITCH_1_event(void)
{
	switch (game_mode)
	{
		case IDLE:
			sys_event |= NEW_GAME;
			break;

		case PLAY:
			if (direction != LEFT)
			{
				if (snake[head].point.x < X_MAX)
				{
					direction = RIGHT;
					sys_event |= MOVE_SNAKE;
				}
			}
			break;

		case NEXT:
			sys_event |= NEXT_LEVEL;
			break;
	}
	return;
} // end SWITCH_1_event


//-- switch #2 event -----------------------------------------------------------
//
void SWITCH_2_event(void)
{
	switch (game_mode)
		{
			case IDLE:
				sys_event |= NEW_GAME;
				break;

			case PLAY:
				if (direction != RIGHT)
				{
					if (snake[head].point.x > 0)
					{
						direction = LEFT;
						sys_event |= MOVE_SNAKE;
					}
				}
				break;

			case NEXT:
				sys_event |= NEXT_LEVEL;
					break;
			}
			return;
} // end SWITCH_2_event


//-- switch #3 event -----------------------------------------------------------
//
void SWITCH_3_event(void)
{
	switch (game_mode)
	{
		case IDLE:
				sys_event |= NEW_GAME;
				break;

			case PLAY:
				if (direction != UP)
				{
					if (snake[head].point.y > 0)
					{
						direction = DOWN;
						sys_event |= MOVE_SNAKE;
					}
				}
				break;

			case NEXT:
				sys_event |= NEXT_LEVEL;
					break;
			}
			return;
} // end SWITCH_3_event


//-- switch #4 event -----------------------------------------------------------
//
void SWITCH_4_event(void)
{
	switch (game_mode)
	{
		case IDLE:
			sys_event |= NEW_GAME;
			break;

		case PLAY:
			if (direction != DOWN)
			{
					if (snake[head].point.y < Y_MAX)
					{
						direction = UP;
						sys_event |= MOVE_SNAKE;
					}
				}
				break;

			case NEXT:
				sys_event |= NEXT_LEVEL;
				break;
			}
			return;
} // end SWITCH_4_event


//-- next level event -----------------------------------------------------------
//
void NEXT_LEVEL_event(void)
{
	game_mode = NO_PLAY;
	free(objects);
	objects = NULL;

	if(level == 4) sys_event |= END_GAME;
	else if(level == 3)
	{
		object_length = LEVEL_4_FOOD;
		move_cnt = WDT_MOVE4;				// level 4, speed 4
		time_remaining  = TIME_4_LIMIT;
	}
	else if(level == 2)
	{
		object_length = LEVEL_3_FOOD;
		move_cnt = WDT_MOVE3;				// level 3, speed 3
		time_remaining  = TIME_3_LIMIT;

	}
	else
	{
		object_length = LEVEL_2_FOOD;
		move_cnt = WDT_MOVE2;				// level 2, speed 2
		time_remaining  = TIME_2_LIMIT;
	}
	level++;
	lcd_clear();
	// Redraw snake
	uint8 j;
	for(j = tail;j != head;j = (j + 1) & (~MAX_SNAKE) )
	{
		lcd_square(COL(snake[j].point.x),
		              ROW(snake[j].point.y),
		              2, 17);
	}
	lcd_square(COL(snake[head].point.x),
			              ROW(snake[head].point.y),
			              2, 17);
	lcd_cursor(0, 0);
	lcd_printf("Up");
	lcd_cursor(37, 0);
	lcd_printf("Down");
	lcd_cursor(97, 0);
	lcd_printf("Left");
	lcd_cursor(130, 0);
	lcd_printf("Right");
	lcd_cursor(0, 150);
	lcd_printf("Score: %d", score);
	lcd_cursor(65, 150);
	lcd_printf("Level %d", level);
	sys_event |= START_LEVEL;
	return;
} // end NEXT_LEVEL_event


//-- update LCD event -----------------------------------------------------------
//
void LCD_UPDATE_event(void)
{
	if(game_mode == IDLE) return;
	if(time_remaining == 0) sys_event |= END_GAME;
	lcd_cursor(0, 150);
	lcd_printf("Score: %d", score);
	lcd_cursor(65, 150);
	lcd_printf("Level %d", level);
	lcd_cursor(135, 150);
	if(time_remaining>=10) lcd_printf("%d:%d", time_remaining/60, time_remaining%60);
	else lcd_printf("0:0%d", time_remaining);
	return;
} // end LCD_UPDATE_event


//-- end game event -------------------------------------------------------------
//
void END_GAME_event(void)
{
	free(objects);
	objects = NULL;
	lcd_clear();
	if(score == 100)
	{
		imperial_march();
		lcd_wordImage(king_snake_image, (159-60)/2, 60, 1);
		game_mode = IDLE;
		return;
	}
	rasberry();
	lcd_wordImage(snake1_image, (159-60)/2, 60, 1);
	lcd_wordImage(snake_text_image, (159-111)/2, 20, 1);
	lcd_cursor(40, 0);
	lcd_printf("Final Score: %d", score);
	game_mode = IDLE;							// Start over
	return;
} // end END_GAME_event

void draw_square(OBJECT* object, uint8 pen)
{
   lcd_square(COL(object->object.point.x),
              ROW(object->object.point.y),
              object->size, pen);
} // end draw_square

void draw_triangle(OBJECT* object, uint8 pen)
{
   lcd_triangle(COL(object->object.point.x),
              ROW(object->object.point.y),
              object->size, pen);
} // end draw_triangle

void draw_diamond(OBJECT* object, uint8 pen)
{
   lcd_diamond(COL(object->object.point.x),
              ROW(object->object.point.y),
              object->size, pen);
} // end draw_diamond
void draw_circle(OBJECT* object, uint8 pen)
{
   lcd_circle(COL(object->object.point.x),
              ROW(object->object.point.y),
              object->size, pen);
} // end draw_circle
void draw_star(OBJECT* object, uint8 pen)
{
	lcd_star(COL(object->object.point.x),
	              ROW(object->object.point.y),
	              object->size, pen);
} // end draw_star


// ****************************************************************
// create and draw an object
//
//      object       *object      (malloc'd)
//    .------.    .------.    .------.
//    | xxxx=|===>| xxxx=|===>| object |
//    '------'    '------'    | size |
//                            |points|
//                            | xxxx=|===>void draw_object(OBJECT*, uint8)
//                            '------'
//
void createObject(OBJECT** object, uint8 x, uint8 y, char c)
{
  // malloc a object struct
  if (!(*object = (OBJECT*)malloc(sizeof(OBJECT)))) ERROR2(SYS_ERR_MALLOC);
  (*object)->type = c;              //   object
  (*object)->object.point.x = x;              // add coordinates to object
  (*object)->object.point.y = y;              //   object
  (*object)->object.xy = ((y << 8) + x);           //   object
  if(c != 'F')
  {
	   (*object)->size = 2;                // 2 pixels in size
	   (*object)->draw = draw_square; // add function pointer to
	   ((*object)->draw)(*object, 17);         // draw object
	   return;
  }
  switch (rand() % 5)                     		// randomly choose a type
  {
     case 0:                              // square food
        (*object)->size = 2;                // 2 pixels in size
        (*object)->draw = draw_square; // add function pointer to
        break;                            //   draw square
     case 1:                              // circle food
        (*object)->size = 2;                // 2 pixels in size
        (*object)->draw = draw_triangle; // add function pointer to
        break;                            //   draw circle
     case 2:                              // triangle food
        (*object)->size = 2;                // 2 pixels in size
        (*object)->draw = draw_diamond; // add function pointer to
        break;                            //   draw triangle
     case 3:                              // diamond food
        (*object)->size = 2;                // 2 pixels in size
        (*object)->draw = draw_circle; // add function pointer to
        break;                            		//   draw diamond
     case 4:                              // star food
        (*object)->size = 2;                // 2 pixels in size
        (*object)->draw = draw_star; // add function pointer to
        break;                            		//   draw star

  }
  ((*object)->draw)(*object, SINGLE);         // draw food
  return;
} // end create food


//-- check_collision function ----------------------------------------------------------
//
// Returns -1 if fatal collision
// Returns 0 if no collisions
// Returns 1 if food collision
int check_collision(uint16 xy, int ifNeedHead)
{
	// Check if hit itself
	uint8 j;
	for(j = tail;j != head;j = (j + 1) & (~MAX_SNAKE) )
	{
		if(xy == snake[j].xy)
		{
			return -1;
		}
	}
	if(ifNeedHead)
	{
		if(xy == snake[head].xy)
		{
			return -1;
		}
	}
	unsigned int i;
	for(i = 0;i < object_length;i++)
	{
		if(xy == (&objects[i])->object.xy)
		{
			// If rock or electric fence
			if((&objects[i])->type == 'R')
			{
				return -1;
			}
			// Is food?
			else if((&objects[i])->type == 'F')
			{
				index_of_replacement = i;
				return 1;
			}
		}
	}
	return 0;
} // end check_collision


//-- move snake event ----------------------------------------------------------
//
void MOVE_SNAKE_event(void)
{
	if (level > 0 && game_mode == PLAY)
	{
		add_head();						// add head
		lcd_point(COL(snake[head].point.x), ROW(snake[head].point.y), PENTX);
		int typeOfCollision = check_collision(snake[head].xy, 0);
		if(typeOfCollision == -1)								// Fatal collision
		{
			sys_event |= END_GAME;
			return;
		}
		else if(typeOfCollision == 1)								//	Food collision
		{
			// Make sure we don't look at it until we are done replacing it
			(&objects[index_of_replacement])->type = 'N';
			// Replace food for all levels but 2
			if(level != 2)
			{
			//	free(objects[index_of_replacement]);
				while(1)
				{
					int randX = (rand() % (X_MAX - 1) + 1);
					int randY = (rand() % (Y_MAX - 1) + 1);
					if(!check_collision(((randY << 8) + randX), 1))
					{
						OBJECT *temp;
						createObject(&temp, randX, randY, 'F');
						objects[index_of_replacement] = *temp;
						free(temp);
						temp = NULL;
						break;
					}
				}
			}
			beep();
			blink();
			score += level;
			sys_event |= LCD_UPDATE;
			if((level == 1 && score >= 10) || (level == 2 && score >= 30) || (level == 3 && score >= 60) || (level == 4 && score >= 100))
			{
				charge();
				game_mode = NEXT;
			}
			return;
		}
		if(!typeOfCollision) delete_tail();					// delete tail if we didn't hit anything
		return;
	}
	return;
} // end MOVE_SNAKE_event


//-- start level event -----------------------------------------------------------
//
void START_LEVEL_event(void)
{
	unsigned numbOfObjectsToAdd = 0;
	if(level > 1) numbOfObjectsToAdd = (rand() % level) + 1;
	if (!(objects = (OBJECT*)malloc(sizeof(OBJECT) * (object_length + numbOfObjectsToAdd)))) ERROR2(SYS_ERR_MALLOC);
	// Draw/Create all food
	unsigned int i = object_length;
	while(i)
	{
		int randX = (rand() % (X_MAX - 1) + 1);
		int randY = (rand() % (Y_MAX - 1) + 1);
		if(!check_collision(((randY << 8) + randX), 1))
		{
			OBJECT *temp;
			createObject(&temp, randX, randY, 'F');
			objects[--i] = *temp;
			free(temp);
			temp = NULL;
		}
	}
	// Create obstacles
	while(numbOfObjectsToAdd)
	{
		int randX = (rand() % (X_MAX - 2) + 1);
		int randY = (rand() % (Y_MAX - 2) + 1);
		if(!check_collision(((randY << 8) + randX) , 1))
		{
			OBJECT *temp;
			createObject(&temp, randX, randY, 'R');
			objects[object_length++] = *temp;
			free(temp);
			temp = NULL;
			numbOfObjectsToAdd--;
		}
	}
	sys_event |= LCD_UPDATE;
	game_mode = PLAY;					// start level
	return;
} // end START_LEVEL_event


//-- init game event ------------------------------------------------------------
//
void INIT_GAME_event(void)
{
	lcd_clear();						// clear lcd
	lcd_backlight(1);					// turn on backlight
	lcd_wordImage(snake1_image, (159-60)/2, 60, 1);
	lcd_wordImage(snake_text_image, (159-111)/2, 20, 1);
	lcd_cursor(0, 0);
	lcd_printf("Up");
	lcd_cursor(37, 0);
	lcd_printf("Down");
	lcd_cursor(97, 0);
	lcd_printf("Left");
	lcd_cursor(130, 0);
	lcd_printf("Right");


	// example foods
	lcd_diamond(COL(16), ROW(20), 2, 1);
	lcd_star(COL(17), ROW(20), 2, 1);
	lcd_circle(COL(18), ROW(20), 2, 1);
	lcd_square(COL(19), ROW(20), 2, 1);
	lcd_triangle(COL(20), ROW(20), 2, 1);
	return;
} // end INIT_GAME_event


//-- new game event ------------------------------------------------------------
//
void NEW_GAME_event(void)
{
	timerB_init();
	level = 1;
	move_cnt = WDT_MOVE1;
	time_remaining = TIME_1_LIMIT;
	object_length = LEVEL_1_FOOD;
	lcd_clear();
	new_snake(START_SCORE, RIGHT);
	score = 0;
	lcd_cursor(0, 0);
	lcd_printf("Up");
	lcd_cursor(37, 0);
	lcd_printf("Down");
	lcd_cursor(97, 0);
	lcd_printf("Left");
	lcd_cursor(130, 0);
	lcd_printf("Right");
	lcd_cursor(0, 150);
	lcd_printf("Score: %d", score);
	lcd_cursor(65, 150);
	lcd_printf("Level %d", level);
	lcd_cursor(135, 150);
	lcd_printf("0:%d", TIME_1_LIMIT);
	sys_event = START_LEVEL;

	return;
} // end NEW_GAME_event


//-- new snake -----------------------------------------------------------------
//
void new_snake(uint16 length, uint8 dir)
{
	int i;
	head = 0;
	tail = 0;
	snake[head].point.x = 0;
	snake[head].point.y = 0;
	direction = dir;

	// build snake
	score = length;
	for (i = score - 1; i; --i)
	{
		add_head();
	}
	return;
} // end new_snake


//-- delete_tail  --------------------------------------------------------------
//
void delete_tail(void)
{
	lcd_point(COL(snake[tail].point.x), ROW(snake[tail].point.y), PENTX_OFF);
	tail = (tail + 1) & (~MAX_SNAKE);
} // end delete_tail


//-- add_head  -----------------------------------------------------------------
//
void add_head(void)
{
	static uint8 (*mFuncs[])(SNAKE*) =	// move head function pointers
	             { move_right, move_up, move_left, move_down };
	uint8 new_head = (head + 1) & (~MAX_SNAKE);
	snake[new_head] = snake[head];		// set new head to previous head
	head = new_head;
	// iterate until valid move
	while ((*mFuncs[direction])(&snake[head]));
} // end add_head


//-- move snake head right -----------------------------------------------------
//
uint8 move_right(SNAKE* head)
{
	if ((head->point.x + 1) < X_MAX)		// room to move right?
	{
		++(head->point.x);					// y, move right
		return FALSE;
	}
	if (level != 2)							// n, right fence
	{
		if (level > 2) sys_event = END_GAME;
		head->point.x = 0;					// wrap around
		return FALSE;
	}
	if (head->point.y) direction = DOWN;	// move up/down
	else direction = UP;
	return TRUE;
} // end move_right


//-- move snake head up --------------------------------------------------------
//
uint8 move_up(SNAKE* head)
{
	if ((head->point.y + 1) < Y_MAX)
	{
		++(head->point.y);					// move up
		return FALSE;
	}
	if (level != 2)							// top fence
	{
		if (level > 2) sys_event = END_GAME;
		head->point.y = 0;					// wrap around
		return FALSE;
	}
	if (head->point.x) direction = LEFT;	// move left/right
	else direction = RIGHT;
	return TRUE;
} // end move_up


//-- move snake head left ------------------------------------------------------
//
uint8 move_left(SNAKE* head)
{
	if (head->point.x)
	{
		--(head->point.x);					// move left
		return FALSE;
	}
	if (level != 2)							// left fence
	{
		if (level > 2) sys_event = END_GAME;
		head->point.x = X_MAX - 1;			// wrap around
		return FALSE;
	}
	if (head->point.y) direction = DOWN;	// move down/up
	else direction = UP;
	return TRUE;
} // end move_left


//-- move snake head down ------------------------------------------------------
//
uint8 move_down(SNAKE* head)
{
	if (head->point.y)
	{
		--(head->point.y);					// move down
		return FALSE;
	}
	if (level != 2)							// bottom fence
	{
		if (level > 2) sys_event = END_GAME;
		head->point.y = Y_MAX - 1;			// wrap around
		return FALSE;
	}
	if (head->point.x) direction = LEFT;	// move left/right
	else direction = RIGHT;
	return TRUE;
} // end move_down
